"""
.. include:: ../../../docs/README_INSTALL.md
.. include:: ../../../docs/README_GUI.md
.. include:: ../../../docs/README_ADD_MODULE.md
.. include:: ../../../docs/README_ADD_CALLBACK.md
.. include:: ../../../docs/README_ADD_REMOTE_SERVER.md
"""
from auto_disc.legacy.base_autodisc_module import BaseAutoDiscModule
from auto_disc.legacy.experiment_pipeline import ExperimentPipeline
