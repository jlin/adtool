from typing import Dict
from auto_disc.auto_disc.systems.System import System
from flow_lenia.main import main as m
from flow_lenia.main import VideoWriter
from flow_lenia.main import *
from auto_disc.auto_disc.utils.expose_config.defaults import (
    Defaults, defaults)
from dataclasses import dataclass
import numpy as np


@dataclass
class FlowParams(Defaults):
    SX: int = defaults(256, min=1, max=2048)
    SY: int = defaults(256, min=1, max=2048)
    nb_k: int = defaults(1, min=1, max=100)
    C: int = defaults(1, min=1, max=100)
    dd: int = defaults(5, min=1, max=100)


@FlowParams.expose_config()
class Interface(System):

    def __init__(self, *args, **kwargs):
        """
        There are no assumptions on the initialization parameters.
        """
        super().__init__()

        # input = {'pre_parameters': {'SX': m.SX ,'SY': m.SY ,'nb_k': m.nb_k, 'C':m.C,'c0': m.c0,'c1':m.c1,
        # ,'dt': m.dt ,'theta': m.theta,'dd' = m.dd,,'sigma': m.sigma}

    def map(self, input: Dict) -> Dict:
        """Map system inputs to outputs.

        A `System` operates on regular Python dicts, but it views them as
        structured. The `premap_key` and `postmap_key` are used to define
        the structured elements that the `System` operates on. Often, the
        `postmap_key` does not exist in the input dict, and is added by the
        `Map` as output.

        Whether or not the `premap_key` exists in the output dict is up to the
        implementation of the specific `System`. We recommend preserving it.

        #### Args
        - input (dict): generic dict containing input data to the map at
            `premap_key`
        #### Returns
        - output (dict): generic dict containing output data from the map at
            `postmap_key`
        """
        param_tensor = input["params"]
        print(f"param_tensor in torch: {param_tensor}")

        SX = 256
        SY = 256
        C = 2
        nb_k = 10
        M = np.ones((C, C), dtype=int) * nb_k
        nb_k = int(M.sum())
        c0, c1 = conn_from_matrix(M)

        dt = param_tensor[0].item()
        theta_A = param_tensor[1].item()
        sigma = param_tensor[2].item()

        config = Config(
            SX=SX,
            SY=SY,
            nb_k=1,
            C=C,
            c0=c0,
            c1=c1,
            dt=dt,
            theta_A=theta_A,
            dd=5,
            sigma=sigma)

        fl = FlowLenia(config)
        roll_fn = jax.jit(fl.rollout_fn, static_argnums=(2,))

        # Initialize state and parameters
        seed = 9  # @param {type : "integer"}
        key = jax.random.PRNGKey(seed)
        params_seed, state_seed = jax.random.split(key)
        params = fl.rule_space.sample(params_seed)
        c_params = fl.kernel_computer(params)  # Process params

        mx, my = SX // 2, SY // 2  # center coordinated
        A0 = jnp.zeros((SX, SY, C)).at[mx - 20:mx + 20, my - 20:my +
                                       20, :].set(jax.random.uniform(state_seed, (40, 40, C)))
        state = State(A=A0)

        print(f"c_params: {c_params}")
        print(f"state: {state}")
        tup = roll_fn(c_params, state, 500)
        states = tup[1]
        input["output"] = states

        return input

    def render(self, data_dict: dict) -> bytes:
        """Render system output as an image or video.

        A `System` should be able to render useful information about its
        execution (e.g., a plot, a video, etc.), which may depend on its
        internal state which is not captured by the `map` method.

        #### Args
        - self (System): If the `System` can render a graphical output without
            needing access to its internal state (i.e., only from the
            output), then it can also be `@classmethod`.
        - data_dict (dict): this is a dict containing the output of the `map`
            method. Depending on implementation, this may be sufficient to
            render the output or one may need stateful data from `self`.
        """
        return b""
        # T = 500
        # with VideoWriter("/tmp/render.mp4", 60) as vid:
        #     for i in range(T):
        #         vid.add(state2img(data_dict['output'][i]))
        # with open("/tmp/render.mp4", "rb") as f:
        #     video_bin = f.read()

        # return video_bin
