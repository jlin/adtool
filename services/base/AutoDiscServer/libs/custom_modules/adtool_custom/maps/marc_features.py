import numpy as np
from .utils import time_diff


def nombre_alternation_montee_descente(d):
    descente = True
    nombre_alternation = 0
    last_value = 1
    for i in d:
        if i < last_value:
            if (not descente):
                nombre_alternation += 1
                descente = True
        else:
            if (descente):
                nombre_alternation += 1
                descente = False
        last_value = i

    return nombre_alternation


def plus_grosse_descente(d):
    montee_actuelle = 0
    montee_max = 0
    last_value = 0
    for i in d:
        if i < last_value:
            montee_actuelle += last_value - i
            montee_max = max(montee_max, montee_actuelle)
        else:
            montee_actuelle = 0
        last_value = i
    return montee_max


def plus_grosse_montee(d):
    montee_actuelle = 0
    montee_max = 0
    last_value = 1
    for i in d:
        if i > last_value:
            montee_actuelle += i - last_value
            montee_max = max(montee_max, montee_actuelle)
        else:
            montee_actuelle = 0
        last_value = i
    return montee_max


def extraire_donnees(d):
    mean = np.mean(d)
    var = np.var(d)
    minimum = np.min(d)
    maximum = np.max(d)
    alternation = nombre_alternation_montee_descente(d)
    montee = plus_grosse_montee(d)
    descente = plus_grosse_descente(d)
    return np.array([mean, var, minimum, maximum, alternation, montee, descente])


def get_descriptor_vector(states_array):
    d = time_diff(states_array, n_frames=500, max_range=2., warmup=100)
    print(f"d: {d}")
    print(f"type(d): {type(d)}")
    return extraire_donnees(d)


