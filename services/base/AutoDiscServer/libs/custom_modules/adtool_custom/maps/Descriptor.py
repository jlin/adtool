from auto_disc.auto_disc.maps.Map import Map
from auto_disc.auto_disc.wrappers.BoxProjector import BoxProjector
import torch
import numpy as np

from .fft_features import get_descriptor_vector as fft_vector
from .marc_features import get_descriptor_vector


class Descriptor(Map):
    def __init__(self, premap_key="output", postmap_key="output"):
        super().__init__()
        self.premap_key = premap_key
        self.postmap_key = postmap_key

        self.projector = BoxProjector(premap_key=self.postmap_key)

    def map(self, input):

        # jp_out = get_descriptor_vector(
        #     input[self.premap_key])  # Insérer descripteurs

        jp_out = fft_vector(
            input[self.premap_key])  # Insérer descripteurs

        np_out = np.asarray(jp_out)

        input['output'] = torch.from_numpy(np_out)

        behavior_dict = self.projector.map(input)

        return behavior_dict

    def sample(self):
        return self.projector.sample()
