import numpy as np
from .utils import time_diff

def bin_fft(x, n_bins):
    each = len(x) / n_bins
    intervals = np.linspace(0, len(x), n_bins)
    return [np.max(x[int(i):int(i+each)]) for i in intervals[:-1]]



def extraire_fourrier(td):
    return bin_fft(np.abs(np.fft.fft(td, 10)), 8)


def get_descriptor_vector(states_array):
    td = time_diff(states_array, n_frames=500, max_range=2., warmup=100)
    return extraire_fourrier(td)

