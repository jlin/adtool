import numpy as np

def time_diff(state, n_frames, max_range, warmup):
    frames=state.A
    norm_factor = np.linalg.norm(np.zeros(frames.shape[-1]) - max_range * np.ones(frames.shape[-1]))
    x = frames[warmup:n_frames:]
    t_diff = (x[1:] - x[:-1]) ** 2
    t_diff /= norm_factor
    t_diff = t_diff.mean(axis=(1, 2))
    return t_diff[:, 0]
