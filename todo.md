Take in a directory of python files which is exported as a package
Take in a conda specification (conda lock)

Load this whole directory into the docker image and build the image

IMGEPExplorer performs a module lookup in the directory and loads the modules
into its factory instance

The `autodiscserver` image is personnalized and is aware of all the user's custom modules
